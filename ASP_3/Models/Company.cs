﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace ASP_3.Models
{
    public class Company
    {
        public int id { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string category_code { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string name { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string area_code { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string currency { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string text_code { get; set; }
        [Required]
        [RegularExpression(@"^[0-9]*$")]
        [StringLength(50, MinimumLength = 2)]
        public string bank_code { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string short_name { get; set; }
    }
}
