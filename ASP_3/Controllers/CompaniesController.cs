﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ASP_3.Models;
using System.Text;
using OfficeOpenXml;
using System.IO;

namespace ASP_3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly CompanyContext _context;

        public CompaniesController(CompanyContext context)
        {
            _context = context;
        }

        // GET: api/Companies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Company>>> Getcompanies()
        {
            return await _context.companies.ToListAsync();
        }

        // GET: api/Companies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Company>> GetCompany(int id)
        {
            var company = await _context.companies.FindAsync(id);

            if (company == null)
            {
                return NotFound();
            }

            return company;
        }

        // PUT: api/Companies/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCompany(int id, Company company)
        {
            if (id != company.id)
            {
                return BadRequest();
            }

            _context.Entry(company).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Companies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Company>> PostCompany(Company company)
        {

            _context.companies.Add(company);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCompany", new { id = company.id }, company);
        }

        //Export CSV file 
        //[HttpPost]
        //public async Task<ActionResult> ExportCSV()
        //{
        //    var companies = await _context.companies.ToListAsync();

        //    //companies.Insert(0, new string[8] { "id", "category_code", "name", "area_code", "currency", "text_code", "bank_code", "short_name" });

        //    System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //    for (int i = 0; i < companies.Count; i++)
        //    {
        //        string[] customer = (string[])companies[i];
        //        for (int j = 0; j < customer.Length; j++)
        //        {
        //            sb.Append(customer[j] + ',');
        //        }

        //        sb.Append("\r\n");

        //    }

        //    return File(System.Text.Encoding.UTF8.GetBytes(sb.ToString()), "text/csv", "Grid.csv");
        //}

        [Route("import")]
        [HttpPost]
        public IActionResult ImportCSVToDB(IFormFile file)
        {
            if (file == null)
            {
                return BadRequest(new ContentResult() { Content = "File upload not found!", StatusCode = 400, ContentType = "application/json" });
            }

            if (file.ContentType == "text/csv")
            {
                try
                {
                    using (var reader = new StreamReader(file.OpenReadStream()))
                    {
                        while (reader.Peek() >= 0)
                        {
                            var rowData = reader.ReadLine();
                            var fieldsData = rowData.Split(',');
                            Company company = new Company();
                            company.category_code = fieldsData[0].Trim();
                            company.name = fieldsData[1].Trim();
                            company.area_code = fieldsData[2].Trim();
                            company.currency = fieldsData[3].Trim();
                            company.text_code = fieldsData[4].Trim();
                            company.bank_code = fieldsData[5].Trim();
                            company.short_name = fieldsData[6].Trim();
                        }
                    }
                    return Ok();
                }
                catch (Exception ex)
                {
                    return StatusCode(500, $"Internal server error: {ex}");
                }
            }
            else if (file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.ContentType == "application/vnd.ms-excel")
            {
                try
                {
                    using (var stream = new MemoryStream())
                    {
                        file.CopyTo(stream);
                        using (var package = new ExcelPackage(stream))
                        {
                            ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                            var rowCount = worksheet.Dimension.Rows;
                            for (int row = 1; row <= rowCount; row++)
                            {
                                Company company = new Company();
                                company.category_code = worksheet.Cells[row, 1].Value.ToString().Trim();
                                company.name = worksheet.Cells[row, 2].Value.ToString().Trim();
                                company.area_code = worksheet.Cells[row, 3].Value.ToString().Trim();
                                company.currency = worksheet.Cells[row, 4].Value.ToString().Trim();
                                company.text_code = worksheet.Cells[row, 5].Value.ToString().Trim();
                                company.bank_code = worksheet.Cells[row, 6].Value.ToString().Trim();
                                company.short_name = worksheet.Cells[row, 7].Value.ToString().Trim();
                            }
                        }
                    }
                    return Ok();
                }
                catch (Exception ex)
                {
                    return StatusCode(500, $"Internal server error: {ex}");
                }
            }
            else
            {
                return BadRequest("hehe");
            }

        }


        // DELETE: api/Companies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCompany(int id)
        {
            var company = await _context.companies.FindAsync(id);
            if (company == null)
            {
                return NotFound();
            }

            _context.companies.Remove(company);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CompanyExists(int id)
        {
            return _context.companies.Any(e => e.id == id);
        }
    }
}
